# Старая версия

```python3 udp_test.py –i 12.12.0.57 –p 5001 -r 700000```

# Новая версия

```python3 udp_test2.py```

Параметры задаются в коде скрипта.

source_ip = '192.168.1.2'  # Замените на ваш исходный IP-адрес

destination_ip = '192.168.1.3'  # Замените на целевой IP-адрес

destination_port = 5001  # Замените на целевой порт (Любой порт целевого хоста без разницы доступен он или нет)

packet_size_bytes = 9997  # Размер пакета в байтах (размер пакета влияет на загрузку CPU)

speed_mbps = 50  # Скорость в Мбит/с (10 Мбит/с)

duration_seconds = 50  # Продолжительность отправки в секундах

dscp = 16  # Значение поля TOS (Type of Service)



# Парметры DSCP

![Снимок экрана 2023-09-17 140845](https://github.com/alexnet123/pics/assets/75438030/6e0f6f6c-0037-43f6-8560-b1052092ae6c)

# Мульти процесс


### Запуск

```

python3 udp_test.py -s 192.168.1.2 -d 192.168.1.1 -P 4
===========================
src_ip:  192.168.1.2
dst_ip:  192.168.1.1
dst_port:  5001
packet_size:  9997  byte
speed:  5  Mb\s
duration:  60  sec
processes:  4
===========================
```

### help

```
python3 udp_test.py -h

```


```
python3 udp_test.py -h
usage: udp_test.py [-h] -s SRC_IP -d DST_IP [-p DST_PORT] [-S PACKET_SIZE]
                   [-b SPEED] [-t DURATION] [-D TOS] [-P PROCESSES]

Утилита для отправки UDP-пакетов с настраиваемыми параметрами.

optional arguments:
  -h, --help            show this help message and exit
  -s SRC_IP, --src_ip SRC_IP
                        Исходный IP-адрес отправителя.
  -d DST_IP, --dst_ip DST_IP
                        Целевой IP-адрес получателя.
  -p DST_PORT, --dst_port DST_PORT
                        Целевой порт получателя. default=5001
  -S PACKET_SIZE, --packet_size PACKET_SIZE
                        Размер пакета в байтах. default=9997
  -b SPEED, --speed SPEED
                        Скорость в Мбит/с. default=5Mb\s
  -t DURATION, --duration DURATION
                        Продолжительность отправки в секундах. default=60sec
  -D TOS, --tos TOS     Значение поля TOS (Type of Service). default=0 Best
                        effort (Precedence 0)
  -P PROCESSES, --processes PROCESSES
                        Количество параллельных процессов. default=1 processes
root@PC-DB:/home/alex#


