import socket
import struct
import time
import argparse

def generate_icmp_packet(src_ip, dst_ip, one_way, packet_size_bytes):
    # Создаем ICMP Echo Request пакет с указанными src и dst IP
    icmp_type = 8  # ICMP Echo Request
    icmp_code = 0
    icmp_checksum = 0
    icmp_identifier = 12345
    icmp_sequence = 1
    icmp_data = b"X" * packet_size_bytes  # Изменяем размер пакета

    if one_way:
        icmp_type = 0  # ICMP Echo Reply

    icmp_header = struct.pack("bbHHh", icmp_type, icmp_code, icmp_checksum, icmp_identifier, icmp_sequence)
    icmp_checksum = calculate_checksum(icmp_header + icmp_data)

    icmp_header = struct.pack("bbHHh", icmp_type, icmp_code, socket.htons(icmp_checksum), icmp_identifier, icmp_sequence)

    icmp_packet = icmp_header + icmp_data

    # Устанавливаем src и dst IP в IP-заголовке
    src_ip_packed = socket.inet_aton(src_ip)
    dst_ip_packed = socket.inet_aton(dst_ip)

    ip_header = struct.pack("!BBHHHBBH4s4s", 69, 0, 28 + len(icmp_packet), 12345, 0, 64, socket.IPPROTO_ICMP, 0, src_ip_packed, dst_ip_packed)

    return ip_header + icmp_packet

def calculate_checksum(data):
    # Вычисляем контрольную сумму ICMP-пакета
    checksum = 0
    for i in range(0, len(data), 2):
        w = (data[i] << 8) + data[i + 1]
        checksum += w
    checksum = (checksum >> 16) + (checksum & 0xFFFF)
    checksum = ~checksum & 0xFFFF
    return socket.htons(checksum)

def send_icmp_packets(src_ip, dst_ip, one_way, speed_mbps, packet_size_bytes):
    icmp_packet = generate_icmp_packet(src_ip, dst_ip, one_way, packet_size_bytes)
    duration_seconds = 100

    # Создаем сокет для отправки ICMP-пакета
    with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW) as sock:

    	# Вычисляем количество пакетов в секунду (PPS) на основе скорости и размера пакета
        pps = (speed_mbps * 10**6) / (packet_size_bytes * 8)

        start_time = time.time()
        end_time = start_time + duration_seconds

        while time.time() < end_time:
            # Рассчитываем интервал между пакетами для достижения нужной скорости
            interval = 1 / pps
            send_time = time.time()

            # Отправляем пакет
            sock.sendto(icmp_packet, (dst_ip, 0))

            # Рассчитываем фактическая скорость передачи на конечном узле (если нужно)
            elapsed_time = time.time() - start_time
            actual_bitrate = (packet_size_bytes * 8 * pps) / (1024 * 1024)  # Мбит/с
            print(f"Скорость передачи ICMP: {actual_bitrate:.2f} Мбит/с")

            # Поддерживаем необходимую скорость
            time.sleep(max(0, interval - (time.time() - send_time)))

    sock.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Генератор ICMP-пакетов.")
    parser.add_argument("--src-ip", required=True, help="IP-адрес отправителя")
    parser.add_argument("--dst-ip", required=True, help="IP-адрес получателя")
    parser.add_argument("--one-way", action="store_true", help="Генерировать односторонние пакеты (без возврата)")
    parser.add_argument("--speed-mbps", type=float, required=True, help="Скорость в Мбит/с")
    parser.add_argument("--packet-size", type=int, default=1470, help="Размер пакета в байтах")

    args = parser.parse_args()

    send_icmp_packets(args.src_ip, args.dst_ip, args.one_way, args.speed_mbps, args.packet_size)
