import socket
import argparse

def check_port(src_ip, dst_ip, dst_port, dscp, timeout=3):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.bind((src_ip, 0))  # Привязываем сокет к исходному IP и случайному порту

    # Установка значения DSCP в поле ToS
    tos = dscp << 2  # DSCP должен быть сдвинут на 2 бита влево
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_TOS, tos)

    try:
        sock.connect((dst_ip, dst_port))
        print(f"Порт {dst_port} на {dst_ip} открыт (исходный IP: {src_ip}).")
        return True
    except socket.error as err:
        print(f"Не удалось подключиться к порту {dst_port} на {dst_ip} с исходного IP {src_ip}: {err}")
        return False
    finally:
        sock.close()

def main():
    parser = argparse.ArgumentParser(description="Проверка TCP порта на удалённом сервере.")
    parser.add_argument("-s", "--src_ip", type=str, required=True, help="Исходный IP-адрес.")
    parser.add_argument("-d", "--dst_ip", type=str, required=True, help="Целевой IP-адрес.")
    parser.add_argument("-p", "--dst_port", type=int, required=True, help="Целевой порт для проверки.")
    parser.add_argument("--dscp", type=int, default=0, help="Значение DSCP.")
    parser.add_argument("-t", "--timeout", type=int, default=3, help="Таймаут для соединения в секундах.")

    args = parser.parse_args()

    check_port(args.src_ip, args.dst_ip, args.dst_port, args.dscp, args.timeout)

if __name__ == "__main__":
    main()
