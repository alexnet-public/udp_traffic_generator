'''
Usage:
  python gt.py –i destinationIp –p port -r bandwidth”
Options:
  - i String  The IP to send the datagram to
  - p Integer The port of the communication
  - r Float   The bandwidth the datagrams are transmitted in

UDP traffic generator
'''

#import protocols.udp as udp
#import shell.options as options
import sys

def getSizeBufferBytes():
    """
    Get defined size buffer in Bytes
    @param: void\n
    @returns: Int
    """
    return 1250

def getSizeBufferBits():
    """
    Get defined size buffer in bits
    @param: void\n
    @returns: Int
    """
    return getSizeBufferBytes()*8

def sendData(ip, port, bandwidth):
    """
    Send data in UDP
    @param: str, int, float\n
    @returns: void
    """
    import socket
    import time

    timeToSend = getSizeBufferBits() *1000/ bandwidth

    import os
    data = os.urandom(1209) # 10000bits
    # Create UDP socket - IPv4
    clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    print("Start sending data over UDP...")
    while (True):
        clientSock.sendto(data, (ip, port))
        time.sleep(timeToSend/1000000.0)

def getArgs(argsLst):
    """
    Get args from command prompt
    @param: list\n
    @returns: obj
    """
    if (argsLst[1] != "-i" and argsLst[3] != "–p" and argsLst[5] != "–r"):
        print("Invalid options given\npython gt.py –i destinationIp –p port -r bandwidth")
        return -1
    else:
        try:
            ip = str(argsLst[2])
            port = int(argsLst[4])
            bandwidth = float(argsLst[6])
        except:
            print("Invalid values given\npython gt.py –i STRING_IP –p INT_PORT -r FLOAT_BANDWIDTH")
            return -1
    return {
        "ip": ip,
        "port": port,
        "bandwidth": bandwidth
    }


def main(args):
    arg = getArgs(args)
    if (arg != -1):
        print("UDP traffic generator: ", arg)
        sendData(arg["ip"], arg["port"], arg["bandwidth"])

if __name__ == '__main__':
    main(sys.argv)
