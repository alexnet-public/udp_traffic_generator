
import socket
import time

def send_udp_packets(src_ip, dst_ip, dst_port, packet_size_bytes, speed_mbps, duration_seconds, tos):
    # Создаем сокет для отправки UDP-пакетов
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_TOS, tos << 2)

    # Создаем данные для отправки (например, 1470 байт данных, как в iperf)
    data = b'x' * packet_size_bytes

    # Вычисляем количество пакетов в секунду (PPS) на основе скорости и размера пакета
    pps = (speed_mbps * 10**6) / (packet_size_bytes * 8)

    start_time = time.time()
    end_time = start_time + duration_seconds

    while time.time() < end_time:
        # Рассчитываем интервал между пакетами для достижения нужной скорости
        interval = 1 / pps
        send_time = time.time()

        # Отправляем пакет
        sock.sendto(data, (dst_ip, dst_port))

        # Рассчитываем фактическая скорость передачи на конечном узле (если нужно)
        elapsed_time = time.time() - start_time
        actual_bitrate = (packet_size_bytes * 8 * pps) / (1024 * 1024)  # Мбит/с
        print(f"Фактическая скорость передачи на конечном узле: {actual_bitrate:.2f} Мбит/с")

        # Поддерживаем необходимую скорость
        time.sleep(max(0, interval - (time.time() - send_time)))

    sock.close()

if __name__ == "__main__":
    # Задаем параметры
    source_ip = '192.168.1.2'  # Замените на ваш исходный IP-адрес
    destination_ip = '192.168.1.3'  # Замените на целевой IP-адрес
    destination_port = 5001  # Замените на целевой порт
    packet_size_bytes = 9997  # Размер пакета в байтах
    speed_mbps = 50  # Скорость в Мбит/с (10 Мбит/с)
    duration_seconds = 50  # Продолжительность отправки в секундах
    dscp = 16  # Значение поля TOS (Type of Service)

    # Запускаем отправку пакетов
    send_udp_packets(source_ip, destination_ip, destination_port, packet_size_bytes, speed_mbps, duration_seconds, dscp)
