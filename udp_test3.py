import socket
import time
import argparse
import multiprocessing
import signal

def send_udp_packets(src_ip, dst_ip, dst_port, packet_size_bytes, speed_mbps, duration_seconds, tos, proc):
    # Создаем сокет для отправки UDP-пакетов
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_TOS, tos << 2)

    # Создаем данные для отправки (например, 1470 байт данных, как в iperf)
    data = b'x' * packet_size_bytes

    # Вычисляем количество пакетов в секунду (PPS) на основе скорости и размера пакета
    pps = (speed_mbps * 10**6) / (packet_size_bytes * 8)

    start_time = time.time()
    end_time = start_time + duration_seconds

    while time.time() < end_time:
        # Рассчитываем интервал между пакетами для достижения нужной скорости
        interval = 1 / pps
        send_time = time.time()

        # Отправляем пакет
        sock.sendto(data, (dst_ip, dst_port))

        # Рассчитываем фактическая скорость передачи на конечном узле (если нужно)
        elapsed_time = time.time() - start_time
        actual_bitrate = (packet_size_bytes * 8 * pps) / (1024 * 1024)  # Мбит/с
        print(f"Скорость передачи: proc({proc}) {actual_bitrate:.2f} Мбит/с")

        # Поддерживаем необходимую скорость
        time.sleep(max(0, interval - (time.time() - send_time)))

    sock.close()

def main():
    parser = argparse.ArgumentParser(description="Утилита для отправки UDP-пакетов с настраиваемыми параметрами.")
    parser.add_argument("-s","--src_ip", type=str, required=True, help="Исходный IP-адрес отправителя.")
    parser.add_argument("-d","--dst_ip", type=str, required=True, help="Целевой IP-адрес получателя.")
    parser.add_argument("-p","--dst_port", type=int, default=5001, help="Целевой порт получателя. default=5001")
    parser.add_argument("-S","--packet_size", type=int, default=9997, help="Размер пакета в байтах. default=9997")
    parser.add_argument("-b","--speed", type=int, default=5, help="Скорость в Мбит/с. default=5Mb\s")
    parser.add_argument("-t","--duration", type=int, default=60, help="Продолжительность отправки в секундах. default=60sec")
    parser.add_argument("-D","--tos", type=int, default=0, help="Значение поля TOS (Type of Service). default=0 Best effort (Precedence 0)")
    parser.add_argument("-P","--processes", type=int, default=1, help="Количество параллельных процессов. default=1 processes")

    args = parser.parse_args()
    print("===========================")
    print("src_ip: ",args.src_ip)
    print("dst_ip: ",args.dst_ip)
    print("dst_port: ",args.dst_port)
    print("packet_size: ",args.packet_size," byte")
    print("speed: ",args.speed, " Mb\s")
    print("duration: ",args.duration, " sec")
    print("processes: ",args.processes)
    print("===========================")
    time.sleep(3)

    # Запускаем несколько параллельных процессов
    i = 0
    processes = []

    # Обработчик сигнала KeyboardInterrupt для завершения процессов при CTRL+C
    def handle_keyboard_interrupt(signal, frame):
        print("Прерывание CTRL+C. Завершение процессов...")
        for process in processes:
            process.terminate()  # Завершаем процесс
            process.join()  # Ждем завершения процесса
        print("Процессы завершены.")
        exit(0)

    # Устанавливаем обработчик сигнала KeyboardInterrupt
    signal.signal(signal.SIGINT, handle_keyboard_interrupt)

    for _ in range(args.processes):
        process = multiprocessing.Process(target=send_udp_packets, args=(args.src_ip, args.dst_ip, args.dst_port, args.packet_size, args.speed, args.duration, args.tos, i))
        processes.append(process)
        process.start()
        i = i + 1
    # Ждем завершения всех процессов
    for process in processes:
        process.join()

if __name__ == "__main__":
    main()
